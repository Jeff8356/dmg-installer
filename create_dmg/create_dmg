#!/bin/zsh
# Zsh has been Mac's default shell for some time.  Future releases of MacOS will likely not include bash
# Usage: create_dmg target_name file1 [file2...]

# TODO: add background image and symlink to /Applications, once we have a signed app and no longer need the README

STAGING_DIR="./Staging"

# Extract the target name
TARGET="$1"
shift

# Look for the .app in the files to be added to the .dmg
APP=""
for src in "$@"
do
    [[ "$src" == *.app ]] && APP="$src"
done

if [[ ${APP} != "" ]]; then
    if [[ -z "$QT_BIN" ]]; then
        echo "Error: QT_BIN must be defined"
        exit 1
    fi

    # Get the version from the application bundle.
    VERSION=`/usr/libexec/PlistBuddy -c "Print CFBundleGetInfoString" ${APP}/Contents/Info.plist`
    echo ${APP} is version ${VERSION}

    # If it's a prerelease version, include the git revision.
    if [[ ${VERSION} == *-* ]]; then
        GIT_REVISION=`/usr/libexec/PlistBuddy -c "Print GitRevision" ${APP}/Contents/Info.plist 2>/dev/null`
        if [[ ${GIT_REVISION} != "" ]]; then
            VERSION=${VERSION}-${GIT_REVISION}
        fi

        # TODO: possibly add -no-strip to macdeployqt for prerelease versions
    fi

    # Create a deployable application bundle (if it hasn't been already been done).
    # Edit: do it every time so that the application gets stripped, just suppress the spurious warnings.
    #if [[ ! -d "${APP}/Contents/Frameworks/QtCore.framework" ]]; then
        echo "${QT_BIN}"/macdeployqt "${APP}"
        "${QT_BIN}"/macdeployqt "${APP}" 2>/dev/null || exit
    #fi
fi

mkdir "${STAGING_DIR}" || exit

for src in "$@"
do
    echo "Copying ${src}"
    cp -a "$src" "${STAGING_DIR}/."
done

echo "Creating temporary .dmg"
# hdiutil create -srcfolder "${STAGING_DIR}" -volname "${TARGET}" -fs HFS+ -fsargs "-c c=64,a=16,e=16" -format UDZO -imagekey zlib-level=9 -o "${TARGET}-${VERSION}.dmg" -ov
#######################
# New code added to make dmg installer
#######################
TARGET_TMP="${TARGET}-${VERSION}-tmp.dmg"
BACKGROUND_IMG="background.png"
VOL_NAME="${TARGET}-${VERSION}-Installer"

# Create the tmp image then mount it so we can copy files to it
DEVICE=$(hdiutil create -srcfolder "${STAGING_DIR}" -volname "${VOL_NAME}" -fs HFS+ -fsargs "-c c=64,a=16,e=16" -format UDRW "${TARGET_TMP}" -ov -attach | egrep '^/dev/' | sed 1q | awk '{print $1}')

#DEVICE=$(hdiutil attach -readwrite -noverify "${TARGET_TMP}" | egrep '^/dev/' | sed 1q | awk '{print $1}')

sleep 2

# Add a link to /Applications
echo "Adding link to /Applications"
pushd /Volumes/"${VOL_NAME}"
ln -s /Applications
popd

# Add a hidden .background folder and copy png to it
echo "Making hidden .background folder and copy the image"
mkdir /Volumes/"${VOL_NAME}"/.background
cp -a ../Building/MacOS/"${BACKGROUND_IMG}" /Volumes/"${VOL_NAME}"/.background/

# tell Finder to resize the window, set the background,
#  change the icon size, place the icons in the right position, etc.
echo "Organizing the installer"
echo '
   tell application "Finder"
     tell disk "'${VOL_NAME}'"
           open
           set current view of container window to icon view
           set pathbar visible of container window to false
           set toolbar visible of container window to false
           set statusbar visible of container window to false
           set the bounds of container window to {400, 100, 1041, 609}
           set viewOptions to the icon view options of container window
           set arrangement of viewOptions to not arranged
           set icon size of viewOptions to 72
           set background picture of viewOptions to file ".background:'${BACKGROUND_IMG}'"
           set position of item "'${APP}'" of container window to {210, 350}
           set position of item "Applications" of container window to {430, 350}
           set position of item "README.rtfd" of container window to {100, 150}
           close
           open
           update without registering applications
           delay 2
     end tell
   end tell
' | osascript

sync

# unmount it
echo "unmounting temporary dmg"
hdiutil detach "${DEVICE}"

# now make the final image for distribution
echo "Creating compressed final image"
hdiutil convert "${TARGET_TMP}" -format UDZO -imagekey zlib-level=9 -o "${TARGET}-${VERSION}" -ov


echo "Cleaning up"
rm -rf "${STAGING_DIR}"
rm -rf "${TARGET_TMP}"

echo "Done"