#!/bin/bash

# Modifications by Jeff8356
# Instructions:
# Make a directory/folder to work in (ie: /Users/jeff/OSCAR-installer)
# In terminal change to the  directory you just created
# Using nano or vi/vim, create the file "make-oscar-installer.sh" and copy the contents of this page into it.
# Using Terminal set the script to be executable (chmod 744 make-oscar-installer.sh).
# (you can also use the .command extension instead of .sh.  The .command extension will
# allow you to double click the script and run it without using Terminal)
# 
# Using Qt, compile OSCAR-code as you normally would.
# From the Deploy build directory, extract OSCAR.app 
# 
# Copy this script and the following files into the OSCAR-installer folder:
#
# OSCAR.app (from the Qt Deploy build)
# README.rtfd
# background.png
#
# Run the script.  It will create temporary files/folders, then delete them when done.
# It will also set the window size/location, icon size/location, create hidden
# .background folder and set the background image.
# There will be a Finder window that opens, just leave it alone, it will close itself.
# Once the script finishes (~30 seconds) you will have a dmg installer for distribution.
# The dmg will have the appropriate version # (and revision if beta/rc).  It gets the info from 
# the plist file in the compiled dmg from Qt.
# 
# Tested on an MBA (Intel) with Big Sur 11.6
#

# make sure we are in the correct dir when we double-click a .command file
dir=${0%/*}
if [ -d "$dir" ]; then
  cd "$dir"
fi

# set up your app name, version number, and background image file name
APP_NAME="OSCAR"
APP_VERSION=`/usr/libexec/PlistBuddy -c "Print CFBundleGetInfoString" ${APP_NAME}.app/Contents/Info.plist`
sleep 5
APP_REVISION=`/usr/libexec/PlistBuddy -c "Print GitRevision" ${APP_NAME}.app/Contents/Info.plist`
# If it's a prerelease version, include the git revision.
    if [[ ${APP_VERSION} == *-* ]]; then
            APP_VERSION=${APP_VERSION}-${APP_REVISION}
        fi

        # TODO: possibly add -no-strip to macdeployqt for prerelease versions

#if [[ ${APP_REVISION} != "" ]]; then
#            APP_VERSION=${APP_VERSION}-${APP_REVISION}
#        fi
        
DMG_BACKGROUND_IMG="background.png"

# you should not need to change these
APP_EXE="${APP_NAME}.app/Contents/MacOS/${APP_NAME}"

VOL_NAME="${APP_NAME}-${APP_VERSION}"
DMG_TMP="${VOL_NAME}-temp.dmg"
DMG_FINAL="${VOL_NAME}.dmg"
STAGING_DIR="./Install"             # we copy all our stuff into this dir

# clear out any old data
rm -rf "${STAGING_DIR}" "${DMG_TMP}" "${DMG_FINAL}"

# copy over the stuff we want in the final disk image to our staging dir
mkdir -p "${STAGING_DIR}"
cp -rpf "${APP_NAME}.app" "${STAGING_DIR}"
cp -rpf README.rtfd "${STAGING_DIR}"
# ... cp anything else you want in the DMG - documentation, etc.

#pushd "${STAGING_DIR}"
#sleep 3
#strip the executable   Is this necessary?
#echo "Stripping ${APP_EXE}..."
#/usr/bin/strip -u -r "${APP_EXE}"
#popd

# create the temp DMG file
hdiutil create -srcfolder "${STAGING_DIR}" -volname "${VOL_NAME}" -fs HFS+ -fsargs "-c c=64,a=16,e=16" -format UDRW "${DMG_TMP}" -ov

echo "Created DMG:"" ${DMG_TMP}"

# mount it and save the device
DEVICE=$(hdiutil attach -readwrite -noverify "${DMG_TMP}" | egrep '^/dev/' | sed 1q | awk '{print $1}')

sleep 2

# add a link to the Applications dir
echo "Add link to /Applications"
pushd /Volumes/"${VOL_NAME}"
ln -s /Applications
popd

# add a background image
mkdir /Volumes/"${VOL_NAME}"/.background
cp "${DMG_BACKGROUND_IMG}" /Volumes/"${VOL_NAME}"/.background/

# tell Finder to resize the window, set the background,
#  change the icon size, place the icons in the right position, etc.
echo '
   tell application "Finder"
     tell disk "'${VOL_NAME}'"
           open
           set current view of container window to icon view
           set pathbar visible of container window to false
           set toolbar visible of container window to false
           set statusbar visible of container window to false
           set the bounds of container window to {400, 100, 1041, 609}
           set viewOptions to the icon view options of container window
           set arrangement of viewOptions to not arranged
           set icon size of viewOptions to 72
           set background picture of viewOptions to file ".background:'${DMG_BACKGROUND_IMG}'"
           set position of item "'${APP_NAME}'.app" of container window to {210, 350}
           set position of item "Applications" of container window to {430, 350}
           set position of item "README.rtfd" of container window to {100, 150}
           close
           open
           update without registering applications
           delay 2
     end tell
   end tell
' | osascript

sync

# unmount it
hdiutil detach "${DEVICE}"

# now make the final image a compressed disk image
echo "Creating compressed image"
hdiutil convert "${DMG_TMP}" -format UDZO -imagekey zlib-level=9 -o "${DMG_FINAL}" -ov

# clean up
rm -rf "${DMG_TMP}"
rm -rf "${STAGING_DIR}"

echo 'Done.'

exit
